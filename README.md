# LMA_IMAGES

LMA_IMAGES is a project with the base Dockerfiles to run ROS 2 foxy and Gazebo on Ubuntu 20.04 Focal.

## Installation

The images are available at dockerhub.com and you can download then looking for the images:
nsdn/ros-foxy-lma:<version>
nsdn/gazebo11-lma:<version>

See all <version> tags available before downloading the images.

## Usage

Those images are used in the scooby_docker_ws project.
Download the source-code of the scooby_docker_ws project at: https://bitbucket.org/nat-sayuri/scooby_docker_ws/

## Clone this repository

If you want to change the files in this project, clone the repository using git clone.

```bash
git clone https://bitbucket.org/nat-sayuri/lma_images.git
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)
#!/bin/bash
set -e

# setup gazebo environment
source "/usr/share/gazebo/setup.sh"

# exporting to bashrc
echo "/usr/share/gazebo/setup.sh" >> ~/.bashrc

exec "$@"

#!/bin/bash
set -e

# setup project environment
source /opt/ros/foxy/setup.bash
source /usr/share/gazebo/setup.sh

# # exporting to bashrc
echo "source /opt/ros/foxy/setup.bash" >> ~/.bashrc
echo "source /usr/share/gazebo/setup.sh" >> ~/.bashrc

exec "$@"
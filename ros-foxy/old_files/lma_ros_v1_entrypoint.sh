#!/bin/bash
set -e

ros_env_setup="$ROS2_WS/install/setup.bash"
echo "sourcing   $ros_env_setup"
source "$ros_env_setup"

echo "ROS_ROOT   $ROS2_WS"
echo "ROS_DISTRO $ROS_DISTRO"

# Exporting to bashrc
echo "source $ROS2_WS/install/setup.bash" >> ~/.bashrc

exec "$@"

#!/bin/bash
set -e

ros_env_setup="/opt/ros/$ROS_DISTRO/install/setup.bash"
echo "sourcing   $ros_env_setup"
source "$ros_env_setup"

echo "ROS_ROOT   $ROS_ROOT"
echo "ROS_DISTRO $ROS_DISTRO"

echo "sourcing   /usr/share/gazebo/setup.sh"

# Exporting to bashrc
echo "source /opt/ros/$ROS_DISTRO/setup.bash" >> ~/.bashrc
echo "source $ROS_ROOT/install/setup.bash" >> ~/.bashrc
echo "source /usr/share/gazebo/setup.sh" >> ~/.bashrc

exec "$@"

# #!/bin/bash
# set -e

# ros_env_setup="$ROS2_WS/install/setup.bash"
# echo "sourcing   $ros_env_setup"
# source "$ros_env_setup"

# echo "ROS_ROOT   $ROS2_WS"
# echo "ROS_DISTRO $ROS_DISTRO"

# # Exporting to bashrc
# echo "source $ROS2_WS/install/setup.bash" >> ~/.bashrc

# exec "$@"
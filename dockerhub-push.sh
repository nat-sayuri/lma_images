### Push to Docker Hub

DOCKER_USER=nsdn
IMG_VERSION=1.0

## 1. Build Gazebo and Ros images

ROOT_PATH=$PWD

# Build Gazebo image
cd $ROOT_PATH/gazebo11
docker build -t ${DOCKER_USER}/gazebo11-lma .
docker image tag ${DOCKER_USER}/gazebo11-lma:latest ${DOCKER_USER}/gazebo11-lma:v${IMG_VERSION}

# Build Ros image
cd $ROOT_PATH/ros-foxy
docker build -t ${DOCKER_USER}/ros-foxy-lma .
docker image tag ${DOCKER_USER}/ros-foxy-lma:latest ${DOCKER_USER}/ros-foxy-lma:v${IMG_VERSION}

# Push images to Docker Hub
docker image push --all-tags ${DOCKER_USER}/gazebo11-lma
docker image push --all-tags ${DOCKER_USER}/ros-foxy-lma

echo "Done pushing images to Docker Hub!"
